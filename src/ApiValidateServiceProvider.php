<?php

namespace Thortech\ApiValidate;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;
use Thortech\ApiValidate\Http\Middleware\After;
use Thortech\ApiValidate\Http\Middleware\Before;

class ApiValidateServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $router = $this->app->make(Router::class);
        $router->pushMiddlewareToGroup('api', After::class);
        $router->pushMiddlewareToGroup('api', Before::class);
    }
}
