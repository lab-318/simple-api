<?php

namespace Thortech\ApiValidate\Http\Middleware;

use Closure;
use Illuminate\Support\Str;

class Before
{
    public function handle($request, Closure $next)
    {
        $header = $request->header('Accept');
        if ($header != 'application/json') {
            return response(['description' => 'only json requests are allowed!'], 406);
        }

        if (!$request->isMethod('post')) return $next($request);

        $header = $request->header('Content-type');
        if (!Str::contains($header, 'application/json')) {
            return response(['description' => 'only json requests are allowed!'], 406);
        }

        $data = $request->json()->all();
        
        if(empty($data['data'])){
            return response(['description' => 'invalid request format!'], 400);
        } 

        return $next($request);
    }


}