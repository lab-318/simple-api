<?php

namespace Thortech\ApiValidate\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Str;

class After
{
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        $content['meta'] = [
            'status' => $response->status(),
            'message' => $response->statusText(),
        ];

        $newArray['data'] = [];
        
        if ($response instanceof JsonResponse) {
            if(empty($response->getData()->data)){
                try{
                    $tempData = (array) $response->getData();

                    // making all data keys camel case
                    foreach($tempData as $key => $value){
                        foreach($value as $key2 => $val){
                            $newArray['data'][$key][Str::camel($key2)] = $val;
                        }
                    }

                }catch(\Throwable $th){
                    $tempData = (array) $response->getData();

                    // making all data keys camel case
                    $keys = array_keys($tempData);
                    $keys = array_map(function($key){
                        return Str::camel($key);
                    }, $keys);
                    $newArray['data'] = array_combine($keys, $tempData);
                }
            }else{

                // moving other addition attribute like paginate to meta attribute
                $tempAnother = collect($response->getData())->except('data')->toArray();
                
                // making all meta keys camel case
                $keys = array_keys($tempAnother);
                $keys = array_map(function($key){
                    return Str::camel($key);
                }, $keys);
                $newContent['meta'] = array_combine($keys, $tempAnother);
                
                $content['meta'] = array_merge($content['meta'], $newContent['meta']);
  
                try {
                    $tempData = $response->getData()->data;

                    // making all data keys camel case
                    foreach($tempData as $key => $value){
                        foreach($value as $key2 => $val){
                            $newArray['data'][$key][Str::camel($key2)] = $val;
                        }
                    }
                } catch (\Throwable $th) {
                    $tempData = $response->getData();

                    // making all data keys camel case
                    foreach($tempData as $key => $value){
                        foreach($value as $key2 => $val){
                            $newArray['data'][$key][Str::camel($key2)] = $val;
                        }
                    }
                }

            }
        
            // jika http status awal 2xx atau 3xx
            if ($response->status() >= 200 && $response->status() <= 399) {
                $content = array_merge($content, $newArray);
            }else{
                $newArray['errors'] = $newArray['data'];
                unset($newArray['data']);
                $content = array_merge($content, $newArray);
            }

            $jsonContent = json_encode($content, JsonResponse::DEFAULT_ENCODING_OPTIONS);
            $response->setContent($jsonContent);

            return $response;
        
        }else{
            $tempData = (array) json_decode($response->getContent());

            // making all data keys camel case
            $keys = array_keys($tempData);
            $keys = array_map(function($key){
                return Str::camel($key);
            }, $keys);
            $newArray['data'] = array_combine($keys, $tempData);

            $response = array_merge($content, $newArray);

            return response($response);
        }

    }


}